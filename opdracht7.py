name = input('Wat is je naam: ')
birth = input('Wat is je geboortejaar: ')
year = input('Vul een willekeurig jaar in: ')


def diff(x1, x2):
    if x1 > x2:
        return x1 - x2
    elif x2 > x1:
        return x2 - x1
    else:
        return 0


print("Hallo " + name + ". In het jaar " + year + " was/wordt je " + str(diff(int(year), int(birth))) + ".")